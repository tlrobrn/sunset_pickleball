defmodule SunsetPickleball.PlayerViewTest do
  use SunsetPickleball.ConnCase
  alias SunsetPickleball.PlayerView

  test "player_data returns the player, EPR, OEPR, and games played" do
    player = insert(:player)
    [9, 11]
    |> Enum.map(
      &insert(:earned_points_ratio, player: player, earned_points: &1, total_points: 20)
    )
    player = Repo.preload(player, [:earned_points_ratios, :games])

    assert {player, 20 / 40, nil, 2} == PlayerView.player_data(player, nil)
  end

  test "player_data returns group data when passed a group" do
    group = insert(:group)
    player = insert(:player_group, group: group).player
    game = insert(:game, group: group)
    insert(:earned_points_ratio, player: player, game: game, earned_points: 4, total_points: 10)
    insert(:earned_points_ratio, player: player, earned_points: 6, total_points: 10)
    player = Repo.preload(player, [:earned_points_ratios, :games])

    assert {player, 4 / 10, nil, 1} == PlayerView.player_data(player, group)
  end

  test "sort_data sorts by EPR desc" do
    ignored_field = (1..4)

    player_data = ["Adam", "Bob", "Charlie", "Doug"]
    |> Stream.map(&insert(:player, first_name: &1))
    |> Stream.zip(4..1)
    |> Stream.zip(ignored_field)
    |> Stream.zip(ignored_field)
    |> Enum.map(fn {{{player, epr}, oepr}, gp} ->
      {player, epr, oepr, gp}
    end)

    sorted = player_data
    |> Enum.shuffle
    |> PlayerView.sort_data

    assert player_data == sorted
  end

  test "sort_data sorts `nil` last" do
    ignored_field = (1..4)

    player_data = ["Adam", "Bob", "Charlie", "Doug"]
    |> Stream.map(&insert(:player, first_name: &1))
    |> Stream.zip([3, 2, 1, nil])
    |> Stream.zip(ignored_field)
    |> Stream.zip(ignored_field)
    |> Enum.map(fn {{{player, epr}, oepr}, gp} ->
      {player, epr, oepr, gp}
    end)

    sorted = player_data
    |> Enum.shuffle
    |> PlayerView.sort_data

    assert player_data == sorted
  end

  test "sort_data sorts by OEPR desc if EPR is equal" do
    ignored_field = (1..4)

    player_data = ["Adam", "Bob", "Charlie", "Doug"]
    |> Stream.map(&insert(:player, first_name: &1))
    |> Stream.map(&{&1, nil})
    |> Stream.zip(4..1)
    |> Stream.zip(ignored_field)
    |> Enum.map(fn {{{player, epr}, oepr}, gp} ->
      {player, epr, oepr, gp}
    end)

    sorted = player_data
    |> Enum.shuffle
    |> PlayerView.sort_data

    assert player_data == sorted
  end

  test "sort_data sorts by games played desc if EPR and OEPR are equal" do
    player_data = ["Adam", "Bob", "Charlie", "Doug"]
    |> Stream.map(&insert(:player, first_name: &1))
    |> Stream.map(&{&1, nil})
    |> Stream.map(&{&1, nil})
    |> Stream.zip(4..1)
    |> Enum.map(fn {{{player, epr}, oepr}, gp} ->
      {player, epr, oepr, gp}
    end)

    sorted = player_data
    |> Enum.shuffle
    |> PlayerView.sort_data

    assert player_data == sorted
  end

  test "sort_data sorts by player name (last, first) ascending as last resort" do
    player_data = ["Adam", "Bob", "Charlie", "Doug"]
    |> Stream.map(&insert(:player, first_name: &1))
    |> Stream.map(&{&1, nil})
    |> Stream.map(&{&1, nil})
    |> Stream.map(&{&1, nil})
    |> Enum.map(fn {{{player, epr}, oepr}, gp} ->
      {player, epr, oepr, gp}
    end)

    sorted = player_data
    |> Enum.shuffle
    |> PlayerView.sort_data

    assert player_data == sorted
  end

  test "display_player displays first name and last name" do
    player = insert(:player, first_name: "Ball", last_name: "Hopkins")
    assert "Ball Hopkins" === PlayerView.display_player(player)
  end

  test "format_epr rounds to 3 decimals (up)" do
    epr = 1.2345
    assert "1.235" == PlayerView.format_epr(epr)
  end

  test "format_epr rounds to 3 decimals (down)" do
    epr = 1.2343
    assert "1.234" == PlayerView.format_epr(epr)
  end

  test "format_epr keeps the leading 0" do
    epr = 0.234
    assert "0.234" == PlayerView.format_epr(epr)
  end

  test "format_epr returns '-----' for `nil`" do
    assert "-----" == PlayerView.format_epr(nil)
  end

  test "ranked_data returns player_data for each player, sorted" do
    players = ["Adam", "Bob", "Charlie", "Doug"]
    |> Stream.map(&insert(:player, first_name: &1))
    |> Stream.map(
      &insert(:earned_points_ratio, player: &1, earned_points: 5, total_points: 5)
    )
    |> Enum.map(&(&1.player))
    |> Repo.preload(:games)

    player_data = players
    |> Stream.with_index
    |> Enum.map(fn {player, index} -> {index + 1, player, 1.0, nil, 1} end)

    assert player_data == Enum.to_list(PlayerView.ranked_data(players, nil))
  end
end
