defmodule SunsetPickleball.GameViewTest do
  use SunsetPickleball.ConnCase
  alias SunsetPickleball.GameView

  test "game_data returns an index, game, group name, team list, and score list" do
    group = insert(:group, name: "A")
    game = insert(:game, group: group)
    score1 = insert(:score, game: game)
    insert(:score, game: game, team: score1.team)
    score2 = insert(:score, game: game)
    game = game |> Repo.preload([:group, :scores, teams: :players])
    [{index, game_result, group_name, teams, scores}] = GameView.game_data([game])

    assert index == 0
    assert game_result.id == game.id
    assert group_name == "A"
    assert Enum.map(teams, &(&1.id)) == [score1.team_id, score2.team_id]
    assert Enum.map(scores, &(&1.id)) == [score1.id, score2.id]
  end

  test "display_players displays a comma separated list of team players" do
    player1 = insert(:player, first_name: "Adam", last_name: "Aaronson")
    player2 = insert(:player, first_name: "Bobby", last_name: "Beauford")
    team = insert(:team, players: [player1, player2])

    assert "Adam Aaronson, Bobby Beauford" == GameView.display_players(team)
  end

  test "display_teams joins the teams with `vs`" do
    team1 = insert(:team)
    team2 = insert(:team)
    result = GameView.display_teams([team1, team2])

    assert String.contains?(result, " vs ")
  end

  test "display_scores joins scores with a `-`" do
    score1 = insert(:score, points: 0)
    score2 = insert(:score, points: 1)
    result = GameView.display_scores([score1, score2])

    assert result == "0 - 1"
  end
end
