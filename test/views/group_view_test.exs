defmodule SunsetPickleball.GroupViewTest do
  use SunsetPickleball.ConnCase
  alias SunsetPickleball.GroupView

  test "player_select_options returns a tuple of player_name and id" do
    player = insert(:player, first_name: "Little", last_name: "Tommy")
    assert GroupView.player_select_options([player]) == [{"Little Tommy", player.id}]
  end
end
