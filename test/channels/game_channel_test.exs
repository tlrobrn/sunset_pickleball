defmodule SunsetPickleball.GameChannelTest do
  use SunsetPickleball.ChannelCase

  alias SunsetPickleball.{EarnedPointsRatio, GameChannel, Repo, Score}

  setup do
    game = insert(:game)
    insert(:score, game: game)
    insert(:score, game: game)

    {:ok, _, socket} =
      socket("user_id", %{some: :assign})
      |> subscribe_and_join(GameChannel, "game:#{game.id}")

    {:ok, socket: socket, game: Repo.preload(game, [:scores, teams: [:players, :scores]])}
  end

  test "join assigns game_id to the socket", %{game: game, socket: socket} do
    assert socket.assigns[:game_id] == game.id
  end

  test "join sends players (with team_id) across the socket", %{game: game} do
    players = game.teams
    |> Stream.flat_map(fn team ->
      team.players
      |> Enum.map(fn player ->
        %{
          "team_id" => team.id,
          "first_name" => player.first_name,
          "last_name" => player.last_name,
        }
      end)
    end)
    |> Enum.uniq

    assert_push "players", %{"players" => ^players}
  end

  test "join sends scores across the socket", %{game: game} do
    scores = game.scores
    |> Enum.map(fn s ->
      %{
        "id" => s.id,
        "points" => s.points,
        "serve" => s.serve,
        "team_id" => s.team_id,
        "game_id" => s.game_id,
      }
    end)

    assert_push "scores", %{"scores" => ^scores}
  end

  test "join sends game_over message if EPRs already exist for the game" do
    game = insert(:earned_points_ratio).game

    socket("user_id", %{some: :assign})
    |> subscribe_and_join(GameChannel, "game:#{game.id}")

    assert_push "game_over", %{}
  end

  test "join does not send the game_over message if no EPRs exist for the game" do
    refute_push "game_over", %{}
  end

  test "ping replies with status ok", %{socket: socket} do
    ref = push socket, "ping", %{"hello" => "there"}
    assert_reply ref, :ok, %{"hello" => "there"}
  end

  test "broadcasts are pushed to the client", %{socket: socket} do
    broadcast_from! socket, "broadcast", %{"some" => "data"}
    assert_push "broadcast", %{"some" => "data"}
  end

  test "replies with an `:ok` message after creating a score", %{socket: socket, game: game} do
    [team | _] = game.teams
    [score | _] = team.scores
    payload = %{
      "id" => score.id,
      "points" => 7,
      "serve" => "second",
      "team_id" => team.id,
      "game_id" => game.id,
    }

    ref = push socket, "scores", %{"scores" => [payload]}
    assert_reply ref, :ok
  end

  test "inserts new scores with the given values upon receiving the \"scores\" message", %{socket: socket, game: game} do
    team = insert(:team)
    payload = %{
      "id" => 1,
      "points" => 7,
      "serve" => "second",
      "team_id" => team.id,
      "game_id" => game.id,
    }

    push socket, "scores", %{"scores" => [payload]}

    assert_broadcast "scores", _response

    score = Repo.one!(
      from s in Score,
      where: s.game_id == ^game.id,
      where: s.team_id == ^team.id,
      where: s.points == ^payload["points"],
      where: s.serve == ^payload["serve"]
    )
    assert payload["id"] != score.id
  end

  test "broadcasts the new scores message upon creating them", %{socket: socket, game: game} do
    [team | _] = game.teams
    [score | _] = team.scores

    payload = %{
      "id" => score.id,
      "points" => 7,
      "serve" => "second",
      "team_id" => team.id,
      "game_id" => game.id,
    }

    push socket, "scores", %{"scores" => [payload]}

    assert_broadcast "scores", %{"scores" => [response]}

    assert payload["points"] == response["points"]
    assert payload["serve"] == response["serve"]
    assert payload["team_id"] == response["team_id"]
    assert payload["game_id"] == response["game_id"]
  end

  test "undo returns `:ok` after deleting the given scores", %{socket: socket, game: game} do
    score = insert(:score, game: game)
    payload = %{
      "id" => score.id,
      "points" => score.points,
      "serve" => score.serve,
      "team_id" => score.team_id,
      "game_id" => score.game_id,
    }
    ref = push socket, "undo", %{"scores" => [payload]}
    assert_reply ref, :ok
  end

  test "undo deletes the given score", %{socket: socket, game: game} do
    score = insert(:score, game: game)
    payload = %{
      "id" => score.id,
      "points" => score.points,
      "serve" => score.serve,
      "team_id" => score.team_id,
      "game_id" => score.game_id,
    }
    ref = push socket, "undo", %{"scores" => [payload]}
    assert_reply ref, :ok

    assert nil == Repo.get(Score, score.id)
  end

  test "undo broadcasts the deleted id to all subscribers", %{socket: socket, game: game} do
    score = insert(:score, game: game)
    payload = %{
      "id" => score.id,
      "points" => score.points,
      "serve" => score.serve,
      "team_id" => score.team_id,
      "game_id" => score.game_id,
    }
    push socket, "undo", %{"scores" => [payload]}
    assert_broadcast "undo", %{"scores" => [^payload]}
  end

  test "game_over returns `:ok` after calculating EPRs", %{socket: socket} do
    ref = push socket, "game_over", %{}
    assert_reply ref, :ok
  end

  test "game_over calculates the EPR given a game", %{socket: socket, game: game} do
    ref = push socket, "game_over", %{}
    assert_reply ref, :ok

    eprs_created = Repo.one(
      from epr in EarnedPointsRatio,
      where: epr.game_id == ^game.id,
      select: count(epr.id)
    )

    assert 4 == eprs_created
  end

  test "game_over broadcasts `game_over` to all the clients`", %{socket: socket} do
    push socket, "game_over", %{}
    assert_broadcast "game_over", %{}
  end
end
