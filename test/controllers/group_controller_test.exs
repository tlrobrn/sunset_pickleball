defmodule SunsetPickleball.GroupControllerTest do
  use SunsetPickleball.ConnCase

  alias SunsetPickleball.Group
  @valid_attrs %{name: "some content"}

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, group_path(conn, :index)
    assert html_response(conn, 200) =~ "Group Rankings"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, group_path(conn, :new)
    assert html_response(conn, 200) =~ "New group"
  end

  test "renders player select box for new resources", %{conn: conn} do
    insert(:player, first_name: "Howie")
    conn = get conn, group_path(conn, :new)
    assert html_response(conn, 200) =~ "Howie"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    player_ids = [insert(:player).id]
    conn = post conn, group_path(conn, :create), group: Map.merge(@valid_attrs, %{player_ids: player_ids, generate_games: false})
    assert redirected_to(conn) == group_path(conn, :index)
    assert Repo.get_by(Group, @valid_attrs)
  end

  test "associates selected players with created group", %{conn: conn} do
    player_ids = [insert(:player).id]
    conn = post conn, group_path(conn, :create), group: Map.merge(@valid_attrs, %{player_ids: player_ids, generate_games: false})
    assert redirected_to(conn) == group_path(conn, :index)

    group = Group
    |> Repo.get_by(@valid_attrs)
    |> Repo.preload(:players)

    assert Enum.map(group.players, &(&1.id)) == player_ids
  end

  test "creates games if generate_games is selected", %{conn: conn} do
    player_ids = [insert(:player).id, insert(:player).id, insert(:player).id, insert(:player).id]
    conn = post conn, group_path(conn, :create), group: Map.merge(@valid_attrs, %{player_ids: player_ids, generate_games: true})
    assert redirected_to(conn) == group_path(conn, :index)

    group = Group
    |> Repo.get_by(@valid_attrs)
    |> Repo.preload(:games)

    assert length(group.games) == 3
  end

  test "shows chosen resource", %{conn: conn} do
    group = Repo.insert! %Group{}
    conn = get conn, group_path(conn, :show, group)
    assert html_response(conn, 200) =~ "Show group"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, group_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    group = Repo.insert! %Group{}
    insert(:player_group, group: group)
    conn = get conn, group_path(conn, :edit, group)
    assert html_response(conn, 200) =~ "Edit group"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    group = Repo.insert! %Group{}
    conn = put conn, group_path(conn, :update, group), group: @valid_attrs
    assert redirected_to(conn) == group_path(conn, :show, group)
    assert Repo.get_by(Group, @valid_attrs)
  end

  test "deletes chosen resource", %{conn: conn} do
    group = Repo.insert! %Group{}
    conn = delete conn, group_path(conn, :delete, group)
    assert redirected_to(conn) == group_path(conn, :index)
    refute Repo.get(Group, group.id)
  end
end
