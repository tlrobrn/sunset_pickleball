defmodule SunsetPickleball.GameControllerTest do
  use SunsetPickleball.ConnCase

  alias SunsetPickleball.Game

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, game_path(conn, :index)
    assert html_response(conn, 200) =~ "Games"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, game_path(conn, :new)
    assert html_response(conn, 200) =~ "New game"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    home_players = [insert(:player), insert(:player)]
    away_players = [insert(:player), insert(:player)]
    conn = post conn, game_path(conn, :create), game: %{home_team: Enum.map(home_players, &(&1.id)), away_team: Enum.map(away_players, &(&1.id))}
    assert redirected_to(conn) == game_path(conn, :index)

    games = home_players ++ away_players
    |> Repo.preload([scores: :game])
    |> Enum.flat_map(fn player ->
      player.scores
      |> Enum.map(&(&1.game))
    end)

    assert length(games) == 4
    assert length(Enum.uniq(games)) == 1
  end

  test "shows chosen resource", %{conn: conn} do
    game = Repo.insert! %Game{}
    conn = get conn, game_path(conn, :show, game)
    assert html_response(conn, 200) =~ "Show game"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, game_path(conn, :show, -1)
    end
  end

  test "deletes chosen resource", %{conn: conn} do
    game = Repo.insert! %Game{}
    conn = delete conn, game_path(conn, :delete, game)
    assert redirected_to(conn) == game_path(conn, :index)
    refute Repo.get(Game, game.id)
  end
end
