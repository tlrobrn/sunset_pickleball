defmodule SunsetPickleball.Factory do
  use ExMachina.Ecto, repo: SunsetPickleball.Repo
  alias SunsetPickleball.{EarnedPointsRatio, Game, Group, Player, PlayerGroup, Roster, Score, Team}

  def earned_points_ratio_factory do
    %EarnedPointsRatio{
      earned_points: 11,
      total_points: 20,
      player: build(:player),
      game: build(:game),
    }
  end

  def game_factory do
    %Game{}
  end

  def group_factory do
    %Group{
      name: sequence("Group"),
    }
  end

  def player_factory do
    %Player{
      first_name: "Pickle",
      last_name: "Watson",
    }
  end

  def player_group_factory do
    %PlayerGroup{
      group: build(:group),
      player: build(:player),
    }
  end

  def roster_factory do
    %Roster{
      team: build(:team),
      player: build(:player),
    }
  end

  def score_factory do
    %Score{
      points: 0,
      serve: "no",
      game: build(:game),
      team: build(:team),
    }
  end

  def team_factory do
    %Team{
      players: [build(:player), build(:player)],
    }
  end
end
