defmodule SunsetPickleball.ScoreTest do
  use SunsetPickleball.ModelCase

  alias SunsetPickleball.Score

  @valid_attrs %{points: 0, serve: "no", game_id: 1, team_id: 1}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Score.changeset(%Score{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Score.changeset(%Score{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "changeset with invalid serve" do
    changeset = Score.changeset(%Score{}, %{points: 0, serve: "not a serve", game_id: 1, team_id: 1})
    refute changeset.valid?
  end

  test "has players" do
    score = insert(:score)
    Repo.preload(score, :players).players
  end

  test "all_for_ids builds a query for scores only with the given ids" do
    score1 = insert(:score)
    score2 = insert(:score)
    insert(:score)

    results = Score.all_for_ids([score1.id, score2.id])
    |> Repo.all
    |> Enum.map(&(&1.id))

    assert [score1.id, score2.id] == results
  end
end
