defmodule SunsetPickleball.TeamTest do
  use SunsetPickleball.ModelCase

  alias SunsetPickleball.Team

  @valid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Team.changeset(%Team{}, @valid_attrs)
    assert changeset.valid?
  end

  test "has scores" do
    team = insert(:team)
    Repo.preload(team, :scores).scores
  end

  test "has players" do
    team = insert(:team)
    Repo.preload(team, :players).players
  end

  test "returns a team if one exists for a list of players" do
    team = insert(:team)
    result = Team.for_players(Team, team.players)
    |> Repo.one
    |> Repo.preload(:players)

    assert team == result
  end

  test "fetch_or_create uses existing team if one exists" do
    team = insert(:team)
    assert Team.fetch_or_create(team.players).id == team.id
  end

  test "fetch_or_create creates a new team if one doesn't exist" do
    players = [insert(:player), insert(:player)]
    Team.fetch_or_create(players)
    assert length(Repo.all(Team)) == 1
  end
end
