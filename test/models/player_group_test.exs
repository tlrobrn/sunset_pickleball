defmodule SunsetPickleball.PlayerGroupTest do
  use SunsetPickleball.ModelCase

  alias SunsetPickleball.PlayerGroup

  @valid_attrs %{player_id: 1, group_id: 1}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = PlayerGroup.changeset(%PlayerGroup{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = PlayerGroup.changeset(%PlayerGroup{}, @invalid_attrs)
    refute changeset.valid?
  end
end
