defmodule SunsetPickleball.GroupTest do
  use SunsetPickleball.ModelCase

  alias SunsetPickleball.Group

  @valid_attrs %{name: "some content"}

  test "changeset with valid attributes" do
    changeset = Group.changeset(%Group{}, @valid_attrs)
    assert changeset.valid?
  end

  test "has player_groups" do
    group = insert(:group)
    player_groups = Repo.preload(group, :player_groups).player_groups
    assert player_groups == []
  end

  test "has games" do
    group = insert(:group)
    games = Repo.preload(group, :games).games
    assert games == []
  end

  test "has players" do
    group = insert(:group)
    players = Repo.preload(group, :players).players
    assert players == []
  end

  test "generate_games creates 3 games for 4 players" do
    group = insert(:group)
    insert(:player_group, group: group)
    insert(:player_group, group: group)
    insert(:player_group, group: group)
    insert(:player_group, group: group)

    games = Group.generate_games(group)
    assert length(games) == 3
  end

  test "generate_games creates 5 games for 5 players" do
    group = insert(:group)
    insert(:player_group, group: group)
    insert(:player_group, group: group)
    insert(:player_group, group: group)
    insert(:player_group, group: group)
    insert(:player_group, group: group)

    games = Group.generate_games(group)
    assert length(games) == 5
  end

  test "generate_games associates the games with the group" do
    group = insert(:group)
    insert(:player_group, group: group)
    insert(:player_group, group: group)
    insert(:player_group, group: group)
    insert(:player_group, group: group)

    game_ids = Group.generate_games(group) |> Enum.map(&(&1.id))
    associated_game_ids = Repo.preload(group, :games).games
    |> Enum.map(&(&1.id))

    assert game_ids == associated_game_ids
  end

  test "generate_groups assigns groups of 5 & 5 for 10 players" do
    players = 1..10 |> Enum.map(fn _ -> insert(:player) end)
    groups = Group.generate_groups(players) |> Repo.preload(:players)

    assert length(groups) == 2
    assert length(hd(groups).players) == 5
  end

  test "generate_groups assigns groups of 4, 4, and 4 for 12 players" do
    players = 1..12 |> Enum.map(fn _ -> insert(:player) end)
    groups = Group.generate_groups(players) |> Repo.preload(:players)

    assert length(groups) == 3
    assert length(hd(groups).players) == 4
  end

  test "generate_groups assigns groups of 5, 4, and 4 for 13 players" do
    players = 1..13 |> Enum.map(fn _ -> insert(:player) end)
    groups = Group.generate_groups(players) |> Repo.preload(:players)

    assert length(groups) == 3

    [g1, g2, g3] = groups
    assert length(g1.players) == 5
    assert length(g2.players) == 4
    assert length(g3.players) == 4
  end

  test "generate_groups assigns groups of 5, 5, and 4 for 14 players" do
    players = 1..14 |> Enum.map(fn _ -> insert(:player) end)
    groups = Group.generate_groups(players) |> Repo.preload(:players)

    assert length(groups) == 3

    [g1, g2, g3] = groups
    assert length(g1.players) == 5
    assert length(g2.players) == 5
    assert length(g3.players) == 4
  end

  test "generate_groups assigns groups of 5, 5, 4, and 4 for 18 players" do
    players = 1..18 |> Enum.map(fn _ -> insert(:player) end)
    groups = Group.generate_groups(players) |> Repo.preload(:players)

    assert length(groups) == 4

    [g1, g2, g3, g4] = groups
    assert length(g1.players) == 5
    assert length(g2.players) == 5
    assert length(g3.players) == 4
    assert length(g4.players) == 4
  end

  test "generate_groups assigns groups of 5, 5, 5, and 4 for 19 players" do
    players = 1..19 |> Enum.map(fn _ -> insert(:player) end)
    groups = Group.generate_groups(players) |> Repo.preload(:players)

    assert length(groups) == 4

    [g1, g2, g3, g4] = groups
    assert length(g1.players) == 5
    assert length(g2.players) == 5
    assert length(g3.players) == 5
    assert length(g4.players) == 4
  end


  test "generate_groups favors groups of 4" do
    players = 1..20 |> Enum.map(fn _ -> insert(:player) end)
    groups = Group.generate_groups(players) |> Repo.preload(:players)

    assert length(groups) == 5
    assert length(hd(groups).players) == 4
  end

  test "associate_players accosiates the given group to the given players" do
    player_ids = [insert(:player).id]
    group = insert(:group)
    |> Group.associate_players(player_ids)
    |> Repo.preload(:players)

    assert player_ids == Enum.map(group.players, &(&1.id))
  end

  test "completed_games returns the number of games completed in a group" do
    group = insert(:group)
    [game1, game2, _] = 1..3 |> Enum.map(fn _ -> insert(:game, group: group) end)
    insert(:earned_points_ratio, game: game1)
    insert(:earned_points_ratio, game: game2)

    assert Group.completed_games(group) |> Enum.map(&(&1.id)) == [game1.id, game2.id]
  end
end
