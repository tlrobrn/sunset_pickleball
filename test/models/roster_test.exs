defmodule SunsetPickleball.RosterTest do
  use SunsetPickleball.ModelCase

  alias SunsetPickleball.Roster

  @valid_attrs %{team_id: 1, player_id: 1}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Roster.changeset(%Roster{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Roster.changeset(%Roster{}, @invalid_attrs)
    refute changeset.valid?
  end
end
