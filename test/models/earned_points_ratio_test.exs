defmodule SunsetPickleball.EarnedPointsRatioTest do
  use SunsetPickleball.ModelCase

  alias SunsetPickleball.EarnedPointsRatio

  @valid_attrs %{earned_points: 42, total_points: 42, player_id: 1, game_id: 1}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = EarnedPointsRatio.changeset(%EarnedPointsRatio{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = EarnedPointsRatio.changeset(%EarnedPointsRatio{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "calculates the EPR for players given a game" do
    team1_points = 11
    team2_points = 9
    total_points = team1_points + team2_points

    game = insert(:game)
    team1 = insert(:team)
    team2 = insert(:team)
    insert(:score, points: team1_points, game: game, team: team1)
    insert(:score, points: team2_points, game: game, team: team2)

    EarnedPointsRatio.calculate(game)

    team1.players
    |> Enum.map(&check_epr_for_player(&1, team1_points, total_points))

    team2.players
    |> Enum.map(&check_epr_for_player(&1, team2_points, total_points))
  end

  defp check_epr_for_player(player, expected_earned, expected_total) do
    epr = Repo.preload(player, :earned_points_ratios).earned_points_ratios
    |> List.first

    assert epr.earned_points == expected_earned
    assert epr.total_points == expected_total
  end
end
