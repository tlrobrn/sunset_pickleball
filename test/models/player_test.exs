defmodule SunsetPickleball.PlayerTest do
  use SunsetPickleball.ModelCase

  alias SunsetPickleball.{Player, Score}

  @valid_attrs %{first_name: "some content", last_name: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Player.changeset(%Player{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Player.changeset(%Player{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "has teams" do
    player = insert(:player)
    teams = Repo.preload(player, :teams).teams
    assert teams == []
  end

  test "has groups" do
    player = insert(:player)
    groups = Repo.preload(player, :groups).groups
    assert groups == []
  end

  test "has scores" do
    player = insert(:player)
    scores = Repo.preload(player, :scores).scores
    assert scores == []
  end

  test "has earned_points_ratios" do
    player = insert(:player)
    earned_points_ratios = Repo.preload(player, :earned_points_ratios).earned_points_ratios
    assert earned_points_ratios == []
  end

  test "has games" do
    player = insert(:player)
    games = Repo.preload(player, :games).games
    assert games == []
  end

  test "total_epr returns the combined EPR" do
    player = insert(:player)
    earned_points_list = [5, 11]
    total_points_list = [16, 18]

    earned_points_list
    |> Stream.zip(total_points_list)
    |> Enum.map(fn {earned_points, total_points} ->
      insert(
        :earned_points_ratio,
        earned_points: earned_points,
        total_points: total_points,
        player: player,
      )
    end)

    expected_epr = Enum.sum(earned_points_list) / Enum.sum(total_points_list)

    assert expected_epr == Player.total_epr(player)
  end

  test "total_epr returns nil if a player hasn't played any games" do
    player = insert(:player)
    assert nil == Player.total_epr(player)
  end

  test "opponents_played returns all the opponents that have played against a player" do
    %Score{team: team, game: game1} = insert(:score)
    game2 = insert(:score, team: team).game

    expected_ids = [game1, game2]
    |> Stream.flat_map(&(insert(:score, game: &1).team.players))
    |> Stream.map(&(&1.id))
    |> Enum.sort

    resulting_ids = team.players
    |> List.first
    |> Player.opponents_played
    |> Stream.map(&(&1.id))
    |> Enum.sort

    assert expected_ids == resulting_ids
  end

  test "opponents_played returns an empty list if a player hasn't played a game" do
    player = insert(:player)
    assert [] == Player.opponents_played(player)
  end

  test "opponents_epr returns the combined OEPR for a player" do
    %Score{team: team, game: game1} = insert(:score)
    game2 = insert(:score, team: team).game
    [game1, game2]
    |> Stream.flat_map(&(insert(:score, game: &1).team.players))
    |> Enum.map(
      &(insert(:earned_points_ratio, player: &1, earned_points: 5, total_points: 10))
    )

    result = team.players
    |> List.first
    |> Player.opponents_epr

    assert 0.5 == result
  end

  test "opponents_epr returns `nil` if a player has no opponents" do
    player = insert(:player)
    assert nil == Player.opponents_epr(player)
  end

  test "group_epr calculates the total epr for only group games" do
    group = insert(:group)
    player = insert(:player_group, group: group).player
    game = insert(:game, group: group)
    insert(:earned_points_ratio, game: game, player: player, earned_points: 5, total_points: 10)
    insert(:earned_points_ratio, player: player, earned_points: 1, total_points: 10)

    assert Player.group_epr(player, group) == 5/10
  end

  test "group_games_played returns the games played by a player in a given group" do
    group = insert(:group)
    player = insert(:player_group, group: group).player
    game = insert(:game, group: group)
    insert(:earned_points_ratio, game: game, player: player, earned_points: 5, total_points: 10)
    insert(:earned_points_ratio, player: player, earned_points: 1, total_points: 10)

    assert Player.group_games_played(player, group) == 1
  end
end
