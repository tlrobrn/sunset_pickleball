defmodule SunsetPickleball.GameTest do
  use SunsetPickleball.ModelCase

  alias SunsetPickleball.Game

  @valid_attrs %{}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Game.changeset(%Game{}, @valid_attrs)
    assert changeset.valid?
  end

  test "has scores" do
    game = insert(:game)
    scores = Repo.preload(game, :scores).scores
    assert scores == []
  end

  test "has earned_points_ratios" do
    game = insert(:game)
    earned_points_ratios = Repo.preload(game, :earned_points_ratios).earned_points_ratios
    assert earned_points_ratios == []
  end

  test "has teams" do
    game = insert(:game)
    teams = Repo.preload(game, :teams).teams
    assert teams == []
  end

  test "has players" do
    game = insert(:game)
    players = Repo.preload(game, :players).players
    assert players == []
  end

  test "get the final score for each team" do
    game = insert(:game)
    team1 = insert(:team)
    team2 = insert(:team)

    score1 = 0..11
    |> Enum.map(&insert(:score, game: game, team: team1, points: &1))
    |> List.last

    score2 = 0..7
    |> Enum.map(&insert(:score, game: game, team: team2, points: &1))
    |> List.last

    assert [score1.id, score2.id] == Enum.map(Game.final_scores(game), &(&1.id))
  end

  test "schedule creates a game and scores for each team" do
    teams = [insert(:team), insert(:team)]
    game = Game.schedule(teams)
    |> Repo.preload(:teams)

    assert Enum.map(game.teams, &(&1.id)) == Enum.map(teams, &(&1.id))
  end

  test "schedule_for_group associates the created game with a group" do
    group = insert(:group)
    teams = [insert(:team), insert(:team)]

    game = Game.schedule_for_group(group, teams)

    assert game.group_id == group.id
  end

  test "upcoming_games returns 1 incomplete game for each group" do
    group1 = insert(:group)
    complete_game = insert(:game, group: group1)
    insert(:earned_points_ratio, game: complete_game)
    games = [insert(:game, group: group1)]

    assert Game.upcoming_games([group1]) |> Enum.map(&(&1.id)) == games |> Enum.map(&(&1.id))
  end

  test "upcoming_games sorts by group with least played" do
    group1 = insert(:group)
    group2 = insert(:group)
    complete_game = insert(:game, group: group1)
    insert(:earned_points_ratio, game: complete_game)
    games = [insert(:game, group: group1), insert(:game, group: group2)]

    assert Game.upcoming_games([group1, group2]) |> Enum.map(&(&1.id)) == games |> Stream.map(&(&1.id)) |> Enum.reverse
  end

  test "upcoming_games sorts by group with most left to play if the groups have played the same amount" do
    group1 = insert(:group)
    group2 = insert(:group)
    [game1, game2, _] = [
      insert(:game, group: group1),
      insert(:game, group: group2),
      insert(:game, group: group2),
    ]

    assert Game.upcoming_games([group1, group2]) |> Enum.map(&(&1.id)) == [game2, game1] |> Enum.map(&(&1.id))
  end
end
