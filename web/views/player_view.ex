defmodule SunsetPickleball.PlayerView do
  use SunsetPickleball.Web, :view
  alias SunsetPickleball.Player

  def ranked_data(players, group) do
    players
    |> Stream.map(&player_data(&1, group))
    |> sort_data
    |> Stream.with_index
    |> Stream.map(fn {{p, epr, oepr, gp}, i} -> {i + 1, p, epr, oepr, gp} end)
  end

  def player_data(%Player{} = player, nil) do
    {
      player,
      Player.total_epr(player),
      Player.opponents_epr(player),
      length(player.games),
    }
  end

  def player_data(%Player{} = player, group) do
    {
      player,
      Player.group_epr(player, group),
      nil,
      Player.group_games_played(player, group),
    }
  end

  def sort_data(data) do
    data
    |> Enum.sort_by(fn {player, _, _, _} -> {player.last_name, player.first_name} end)
    |> Enum.sort_by(fn {_, _, _, gp} -> gp end, &>=/2)
    |> Enum.sort_by(fn {_, _, oepr, _} -> oepr || -1.0 end, &>=/2)
    |> Enum.sort_by(fn {_, epr, _, _} -> epr || -1.0 end, &>=/2)
  end

  def display_player(%Player{first_name: first_name, last_name: last_name}) do
    "#{first_name} #{last_name}"
  end

  def format_epr(nil), do: "-----"
  def format_epr(epr), do: Float.to_string(epr, decimals: 3)
end
