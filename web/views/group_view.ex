defmodule SunsetPickleball.GroupView do
  use SunsetPickleball.Web, :view

  def player_select_options(players) do
    players
    |> Enum.map(fn player ->
      {player_name(player), player.id}
    end)
  end

  defp player_name(player), do: "#{player.first_name} #{player.last_name}"
end
