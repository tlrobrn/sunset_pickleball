defmodule SunsetPickleball.GameView do
  use SunsetPickleball.Web, :view
  alias SunsetPickleball.Game

  def game_data(games) do
    games
    |> Stream.with_index
    |> Enum.map(&build_data_tuple/1)
  end

  defp build_data_tuple({game, index}) do
    group_name = case game.group do
                   nil -> ""
                   group -> group.name
                 end
    {
      index,
      game,
      group_name,
      game.teams |> Stream.uniq |> Enum.sort_by(&(&1.id)),
      Game.final_scores(game) |> Enum.sort_by(&(&1.team_id)),
    }
  end

  def display_players(team) do
    team.players
    |> Enum.sort_by(&(&1.last_name))
    |> Stream.map(&display_player/1)
    |> Enum.join(", ")
  end

  def display_teams(teams) do
    teams
    |> Stream.map(&display_players/1)
    |> Enum.join(" vs ")
  end

  def display_scores(scores) do
    scores |> Stream.map(&(&1.points))|> Enum.join(" - ")
  end

  defp display_player(player) do
    "#{player.first_name} #{player.last_name}"
  end

  def player_select_options(players) do
    players
    |> Enum.map(fn player ->
      {display_player(player), player.id}
    end)
  end
end
