import Player exposing ( Player )
import Score
import Scoreboard exposing ( .. )
import Html exposing ( Html, button, div, text )
import Html.App as App
import Html.Attributes exposing ( class, classList, disabled )
import Html.Events exposing ( onClick )
import Json.Decode as JD
import Json.Encode as JE
import Phoenix.Channel
import Phoenix.Push
import Phoenix.Socket


main : Program Flags
main =
    App.programWithFlags
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }

-- MODEL

type alias Model =
    { phxSocket : Phoenix.Socket.Socket Msg
    , scoreboard : Scoreboard
    , players : List Player
    , topic : String
    , gameEnded : Bool
    }


type alias Flags =
    { protocol : String
    , host : String
    , gameId : String
    }


initPhxSocket : String -> String -> Phoenix.Socket.Socket Msg
initPhxSocket socketServer topic =
    Phoenix.Socket.init socketServer
        |> Phoenix.Socket.withDebug
        |> Phoenix.Socket.on "scores" topic AddScores
        |> Phoenix.Socket.on "undo" topic DeleteScores
        |> Phoenix.Socket.on "players" topic SetPlayers
        |> Phoenix.Socket.on "game_over" topic SetGameEnded


initModel : Flags -> Model
initModel { protocol, host, gameId } =
    let
        socketServer = protocol ++ "://" ++ host ++ "/socket/websocket"
        topic = "game:" ++ gameId

    in
        { phxSocket = initPhxSocket socketServer topic
        , scoreboard = initScoreboard
        , players = []
        , topic = topic
        , gameEnded = False
        }


init : Flags -> ( Model, Cmd Msg )
init flags =
    let
        model = initModel flags
        channel = Phoenix.Channel.init model.topic
        ( phxSocket, phxCmd ) = Phoenix.Socket.join channel model.phxSocket

    in
        ( { model | phxSocket = phxSocket }
        , Cmd.map PhoenixMsg phxCmd
        )

-- SUBSCRIPTIONS

subscriptions : Model -> Sub Msg
subscriptions model =
    Phoenix.Socket.listen model.phxSocket PhoenixMsg

-- UPDATE

type Msg
    = PhoenixMsg ( Phoenix.Socket.Msg Msg )
    | EndVolley Side
    | Undo
    | GameOver
    | Switch
    | AddScores JE.Value
    | DeleteScores JE.Value
    | SetPlayers JE.Value
    | SetGameEnded JE.Value


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        PhoenixMsg msg ->
            let
                ( phxSocket, phxCmd ) = Phoenix.Socket.update msg model.phxSocket

            in
                ( { model | phxSocket = phxSocket }
                , Cmd.map PhoenixMsg phxCmd
                )

        EndVolley winningSide ->
            if model.scoreboard.complete then
                ( model, Cmd.none )

            else
                let
                    newScores = updateScores winningSide model.scoreboard
                    payload = Score.jsonEncoder newScores
                    push' =
                        Phoenix.Push.init "scores" model.topic
                            |> Phoenix.Push.withPayload payload
                    ( phxSocket, phxCmd ) = Phoenix.Socket.push push' model.phxSocket

                in
                    ( { model | phxSocket = phxSocket }
                    , Cmd.map PhoenixMsg phxCmd
                    )

        Undo ->
            if model.gameEnded then
                ( model, Cmd.none )

            else
                let
                    deletedScores = undoScores model.scoreboard
                    payload = Score.jsonEncoder deletedScores
                    push' =
                        Phoenix.Push.init "undo" model.topic
                            |> Phoenix.Push.withPayload payload
                    ( phxSocket, phxCmd ) = Phoenix.Socket.push push' model.phxSocket

                in
                    ( { model | phxSocket = phxSocket }
                    , Cmd.map PhoenixMsg phxCmd
                    )

        GameOver ->
            if model.gameEnded then
                ( model, Cmd.none )

            else
                let
                    push' = Phoenix.Push.init "game_over" model.topic
                    ( phxSocket, phxCmd ) = Phoenix.Socket.push push' model.phxSocket

                in
                    ( { model | phxSocket = phxSocket }
                    , Cmd.map PhoenixMsg phxCmd
                    )

        Switch ->
            let
                scoreboard = switchSides model.scoreboard

            in
                ( { model | scoreboard = scoreboard }
                , Cmd.none
                )

        AddScores msg ->
            case JD.decodeValue Score.jsonDecoder msg of
                Ok newScores ->
                    ( { model | scoreboard = addScores newScores model.scoreboard }
                    , Cmd.none
                    )

                Err error ->
                    ( model, Cmd.none )

        DeleteScores msg ->
            case JD.decodeValue Score.jsonDecoder msg of
                Ok deletedScores ->
                    ( { model | scoreboard = deleteScores deletedScores model.scoreboard }
                    , Cmd.none
                    )

                Err error ->
                    ( model, Cmd.none )

        SetPlayers msg ->
            case JD.decodeValue Player.jsonDecoder msg of
                Ok players ->
                    ( { model | players = players }
                    , Cmd.none
                    )

                Err error ->
                    ( model, Cmd.none )

        SetGameEnded msg ->
            ( { model | gameEnded = True }, Cmd.none )

-- VIEW

view : Model -> Html Msg
view model =
    let
        scoreboard = model.scoreboard
        players = model.players
        leftScore = Score.viewCurrentScore scoreboard.leftList
        rightScore = Score.viewCurrentScore scoreboard.rightList
        leftTeam = Scoreboard.leftTeam scoreboard
        rightTeam = Scoreboard.rightTeam scoreboard
        leftPlayers = Player.forTeam leftTeam players
        rightPlayers = Player.forTeam rightTeam players

    in
        div []
            [ div [ class "scoreboard-side col-xs-6" ]
                  [ div [ class "players" ] ( teamHtml leftPlayers )
                  , button
                        [ onClick ( EndVolley Left )
                        , classList
                              [ ( "score", True )
                              , ( "game-ended", model.gameEnded )
                              ]
                        , disabled ( scoreboard.complete || model.gameEnded )
                        ]
                        [ text leftScore ]
                  ]
            , div [ class "scoreboard-side col-xs-6" ]
                  [ div [ class "players" ] ( teamHtml rightPlayers )
                  , button
                        [ onClick ( EndVolley Right )
                        , classList
                              [ ( "score", True )
                              , ( "game-ended", model.gameEnded )
                              ]
                        , disabled ( scoreboard.complete || model.gameEnded )
                        ]
                        [ text rightScore ]
                  ]
            , div []
                  [ button [ onClick Switch , class "scoreboard-btn" ] [ text "Switch" ]
                  , button
                        [ onClick Undo
                        , class "scoreboard-btn"
                        , disabled model.gameEnded
                        ]
                        [ text "Undo" ]
                  , button
                        [ onClick GameOver
                        , class "scoreboard-btn"
                        , disabled ( not ( scoreboard.complete && (not model.gameEnded ) ) )
                        ]
                        [ text "End Game" ]
                  ]
            ]


playerHtml : Player -> Html Msg
playerHtml player =
    let textNode =
        player
            |> Player.displayName
            |> text

    in
        div [] [ textNode ]

teamHtml : List Player -> List ( Html Msg )
teamHtml =
    List.map playerHtml
