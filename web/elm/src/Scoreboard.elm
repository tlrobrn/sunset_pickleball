module Scoreboard exposing
    ( Scoreboard
    , Side( .. )
    , initScoreboard
    , updateScores
    , undoScores
    , switchSides
    , addScores
    , deleteScores
    , leftTeam
    , rightTeam
    )


import Score exposing ( .. )


type Side
    = Left
    | Right


type Event
    = AddLeft
    | AddRight
    | AddBoth


type alias Scoreboard =
    { leftList : List Score
    , rightList : List Score
    , events : List Event
    , complete : Bool
    }


initScoreboard : Scoreboard
initScoreboard =
    Scoreboard [] [] [] False


updateScores : Side -> Scoreboard -> List Score
updateScores winningSide scoreboard =
    case winningSide of
        Left ->
            calculateNewScores scoreboard.leftList scoreboard.rightList
        Right ->
            calculateNewScores scoreboard.rightList scoreboard.leftList


undoScores : Scoreboard -> List Score
undoScores scoreboard =
    case ( scoreboard.leftList, scoreboard.rightList ) of
        ( [], _ ) ->
            []

        ( _, [] ) ->
            []

        ( left::_, right::_ ) ->
          case scoreboard.events of
              AddLeft::_ ->
                  [ left ]

              AddRight::_ ->
                  [ right ]

              AddBoth::_ ->
                  [ left, right ]

              [] ->
                  []


switchEvent : Event -> Event
switchEvent event =
    case event of
        AddLeft -> AddRight
        AddRight -> AddLeft
        AddBoth -> AddBoth


switchEvents : List Event -> List Event
switchEvents = List.map switchEvent


switchSides : Scoreboard -> Scoreboard
switchSides {leftList, rightList, events, complete} =
    { leftList = rightList
    , rightList = leftList
    , events = switchEvents events
    , complete = complete
    }


determineEvent : List Score -> List Score ->  Scoreboard -> Event
determineEvent leftList rightList scoreboard =
    let
        differentLeft = ( leftList /= scoreboard.leftList )
        differentRight = ( rightList /= scoreboard.rightList )

    in
        case ( differentLeft, differentRight ) of
            ( True, False ) -> AddLeft
            ( False, True ) -> AddRight
            ( True, True ) -> AddBoth
            _ -> AddBoth


addScores : List Score -> Scoreboard -> Scoreboard
addScores scores scoreboard =
    case ( scoreboard.leftList, scoreboard.rightList ) of
        ( [], [] ) ->
            initialScores scores scoreboard

        _ ->
          let
              leftList = addScoresToList scoreboard.leftList scores
              rightList = addScoresToList scoreboard.rightList scores
              event = determineEvent leftList rightList scoreboard

          in
              { leftList = leftList
              , rightList = rightList
              , events = event::scoreboard.events
              , complete = isComplete leftList rightList
              }


initialScores : List Score -> Scoreboard -> Scoreboard
initialScores scores scoreboard =
    case scores of
        head::_ ->
            let
                ( leftList', rightList' ) =
                    List.partition (\s -> s.team_id == head.team_id) scores
                leftList = List.reverse leftList'
                rightList = List.reverse rightList'

            in
                { scoreboard
                | leftList = leftList
                , rightList = rightList
                , complete = isComplete leftList rightList
                }

        [] ->
            scoreboard



deleteScores : List Score -> Scoreboard -> Scoreboard
deleteScores scores scoreboard =
    case scoreboard.events of
        [] -> scoreboard
        _::events ->
            let
                leftList = deleteScoresFromList scoreboard.leftList scores
                rightList = deleteScoresFromList scoreboard.rightList scores

            in
                { leftList = leftList
                , rightList = rightList
                , events = events
                , complete = isComplete leftList rightList
                }


leftTeam : Scoreboard -> Int
leftTeam = teamFor Left


rightTeam : Scoreboard -> Int
rightTeam = teamFor Right


teamFor : Side -> Scoreboard -> Int
teamFor side scoreboard =
    let
        left = currentScore scoreboard.leftList
        right = currentScore scoreboard.rightList

    in
        case side of
            Left -> left.team_id
            Right -> right.team_id
