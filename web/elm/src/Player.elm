module Player exposing
    ( Player
    , displayName
    , jsonDecoder
    , forTeam
    )

import String exposing ( left, concat )
import Json.Decode as JD exposing ( (:=) )


type alias Player =
    { team_id : Int
    , first_name : String
    , last_name : String
    }


playerDecoder : JD.Decoder Player
playerDecoder =
    JD.object3 Player
        ( "team_id" := JD.int )
        ( "first_name" := JD.string )
        ( "last_name" := JD.string )


jsonDecoder : JD.Decoder ( List Player )
jsonDecoder =
    JD.at [ "players" ] ( JD.list playerDecoder )


displayName : Player -> String
displayName player =
    let
        first_name = String.toUpper player.first_name
        last_name = String.toUpper player.last_name

    in
        concat [ first_name, " ", last_name ]


forTeam : Int -> List Player -> List Player
forTeam teamId =
    List.filter ( \player -> player.team_id == teamId )
