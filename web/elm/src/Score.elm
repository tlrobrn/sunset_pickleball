module Score exposing
    ( Score
    , addScoresToList
    , deleteScoresFromList
    , isComplete
    , calculateNewScores
    , jsonDecoder
    , jsonEncoder
    , currentScore
    , viewCurrentScore
    )


import Json.Decode as JD exposing ( (:=) )
import Json.Encode as JE


type Serve
    = First
    | Second
    | No


type alias Score =
    { id : Int
    , points : Int
    , serve : Serve
    , game_id : Int
    , team_id : Int
    }


emptyScore : Score
emptyScore =
    { id = -1
    , points = -1
    , serve = No
    , game_id = -1
    , team_id = -1
    }


scoreDecoderByServe : String -> JD.Decoder Score
scoreDecoderByServe serve =
    case serve of
        "first" ->
            JD.object5 Score
                ( "id" := JD.int )
                ( "points" := JD.int )
                ( JD.succeed First )
                ( "game_id" := JD.int )
                ( "team_id" := JD.int )

        "second" ->
            JD.object5 Score
                ( "id" := JD.int )
                ( "points" := JD.int )
                ( JD.succeed Second )
                ( "game_id" := JD.int )
                ( "team_id" := JD.int )

        "no" ->
            JD.object5 Score
                ( "id" := JD.int )
                ( "points" := JD.int )
                ( JD.succeed No )
                ( "game_id" := JD.int )
                ( "team_id" := JD.int )

        _ ->
            JD.fail "Invalid serve"



scoreDecoder : JD.Decoder Score
scoreDecoder =
    ( "serve" := JD.string ) `JD.andThen` scoreDecoderByServe


jsonDecoder : JD.Decoder ( List Score )
jsonDecoder =
    JD.at [ "scores" ] ( JD.list scoreDecoder )


serveEncoder : Serve -> JE.Value
serveEncoder serve =
    case serve of
        First ->
            JE.string "first"

        Second ->
            JE.string "second"

        No ->
            JE.string "no"


scoreEncoder : Score -> JE.Value
scoreEncoder score =
    JE.object
        [ ( "id", JE.int score.id )
        , ( "points", JE.int score.points )
        , ( "serve", serveEncoder score.serve )
        , ( "game_id", JE.int score.game_id )
        , ( "team_id", JE.int score.team_id )
        ]


jsonEncoder : List Score -> JE.Value
jsonEncoder scores =
    let scoresJson = List.map scoreEncoder scores

    in
        JE.object
            [ ( "scores", JE.list scoresJson ) ]


currentScore : List Score -> Score
currentScore scores =
    Maybe.withDefault emptyScore ( List.head scores )


calculateNewScores : List Score -> List Score -> List Score
calculateNewScores wonScores lostScores =
    let
        won = currentScore wonScores
        lost = currentScore lostScores

    in
        case ( won.serve, lost.serve ) of
            ( No, Second ) ->
                [ { won | serve = First }
                , { lost | serve = No }
                ]

            ( No, First ) ->
                [ { lost | serve = Second } ]

            ( _, No ) ->
                [ { won | points = won.points + 1 } ]

            ( _, _ ) ->
                []


isComplete : List Score -> List Score -> Bool
isComplete leftList rightList =
    let
        left = currentScore leftList
        right = currentScore rightList
        leftPoints = left.points
        rightPoints = right.points

    in
        ( max leftPoints rightPoints ) >= 11
            && ( abs ( leftPoints - rightPoints ) ) >= 2


addScoreToList : Score -> List Score -> List Score
addScoreToList score list =
    case list of
        head::_ ->
          if score.team_id == head.team_id then
              score::list
          else
              list

        [] ->
            [ score ]


addScoresToList : List Score -> List Score -> List Score
addScoresToList existingScores newScores =
    List.foldr addScoreToList existingScores newScores


deleteScoreFromList : Score -> List Score -> List Score
deleteScoreFromList score list =
    case list of
        head::tail ->
            if head.id == score.id then
                tail
            else
                list

        [] ->
            list


deleteScoresFromList : List Score -> List Score -> List Score
deleteScoresFromList existingScores deletedScores =
    List.foldr deleteScoreFromList existingScores deletedScores


viewCurrentScore : List Score -> String
viewCurrentScore scores =
    case scores of
        score::tail ->
            let
                pointsString = toString score.points
                serveString =
                    case score.serve of
                        No -> ""
                        First -> "."
                        Second -> ":"

            in
                pointsString ++ serveString

        [] ->
            ""
