defmodule SunsetPickleball.GroupController do
  use SunsetPickleball.Web, :controller

  alias SunsetPickleball.{Group, Player}

  def index(conn, _params) do
    groups = Repo.all(Group)
    |> Repo.preload(:players)
    render(conn, "index.html", groups: groups)
  end

  def new(conn, _params) do
    changeset = Group.changeset(%Group{})
    players = Repo.all(Player)
    render(conn, "new.html", changeset: changeset, players: players)
  end

  def create(conn, %{"group" => %{"player_ids" => player_ids, "name" => name, "generate_games" => generate_games}}) do
    changeset = Group.changeset(%Group{}, %{"name" => name})

    case Repo.insert(changeset) do
      {:ok, group} ->
        Group.associate_players(group, player_ids)
        if generate_games, do: Group.generate_games(group)

        conn
        |> put_flash(:info, "Group created successfully.")
        |> redirect(to: group_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def generate(conn, _params) do
    players = Repo.all(Player)
    render(conn, "generate.html", players: players)
  end

  def generate_groups(conn, _params) do
    if conn.params["generate"]["all_players"] do
      Player
    else
      player_ids = conn.params["generate"]["player_ids"]
      (from p in Player, where: p.id in ^player_ids)
    end
    |> Repo.all
    |> generate_groups_for_players(conn)
  end

  defp generate_groups_for_players(players, conn) do
    players
    |> Group.generate_groups
    |> Enum.each(&Group.generate_games/1)

    conn
    |> put_flash(:info, "Groups generated successfully")
    |> redirect(to: group_path(conn, :index))
  end

  def show(conn, %{"id" => id}) do
    group = Repo.get!(Group, id)
    render(conn, "show.html", group: group)
  end

  def edit(conn, %{"id" => id}) do
    players = Repo.all(Player)
    group = Repo.get!(Group, id)
    changeset = Group.changeset(group)
    render(conn, "edit.html", group: group, changeset: changeset, players: players)
  end

  def update(conn, %{"id" => id, "group" => group_params}) do
    group = Repo.get!(Group, id)
    changeset = Group.changeset(group, group_params)

    case Repo.update(changeset) do
      {:ok, group} ->
        conn
        |> put_flash(:info, "Group updated successfully.")
        |> redirect(to: group_path(conn, :show, group))
      {:error, changeset} ->
        render(conn, "edit.html", group: group, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    group = Repo.get!(Group, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(group)

    conn
    |> put_flash(:info, "Group deleted successfully.")
    |> redirect(to: group_path(conn, :index))
  end
end
