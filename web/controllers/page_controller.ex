defmodule SunsetPickleball.PageController do
  use SunsetPickleball.Web, :controller
  alias SunsetPickleball.{Game, Group}

  def index(conn, _params) do
    groups = Repo.all(Group)
    |> Repo.preload(:players)

    games = Game.upcoming_games(groups)
    |> Repo.preload([:group, :scores, teams: :players])

    render(conn, "index.html", groups: groups, games: games)
  end
end
