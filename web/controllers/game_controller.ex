defmodule SunsetPickleball.GameController do
  use SunsetPickleball.Web, :controller

  alias SunsetPickleball.{Game, Player, Team}

  def index(conn, _params) do
    games = Repo.all(Game)
    |> Repo.preload([:group, :scores, teams: :players])

    render(conn, "index.html", games: games)
  end

  def new(conn, _params) do
    changeset = Game.changeset(%Game{})
    players = Repo.all(Player)
    render(conn, "new.html", changeset: changeset, players: players)
  end

  def create(conn, %{"game" => %{"home_team" => home_player_ids, "away_team" => away_player_ids}}) do
    home_team = home_player_ids
    |> Enum.map(&Repo.get(Player, &1))
    |> Team.fetch_or_create

    away_team = away_player_ids
    |> Enum.map(&Repo.get(Player, &1))
    |> Team.fetch_or_create

    Game.schedule([home_team, away_team])

    conn
    |> put_flash(:info, "Game created successfully.")
    |> redirect(to: game_path(conn, :index))
  end

  def show(conn, %{"id" => id}) do
    game = Repo.get!(Game, id)
    render(conn, "show.html", game: game)
  end

  def edit(conn, %{"id" => id}) do
    game = Repo.get!(Game, id)
    changeset = Game.changeset(game)
    render(
      conn,
      "edit.html",
      game: game,
      changeset: changeset,
      layout: {SunsetPickleball.LayoutView, "scoreboard.html"}
    )
  end

  def delete(conn, %{"id" => id}) do
    game = Repo.get!(Game, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(game)

    conn
    |> put_flash(:info, "Game deleted successfully.")
    |> redirect(to: game_path(conn, :index))
  end
end
