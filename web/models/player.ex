defmodule SunsetPickleball.Player do
  use SunsetPickleball.Web, :model
  alias SunsetPickleball.{Player, Repo, Roster, Score}

  schema "players" do
    field :first_name, :string
    field :last_name, :string

    timestamps()

    many_to_many :teams, SunsetPickleball.Team, join_through: SunsetPickleball.Roster
    has_many :scores, through: [:teams, :scores]
    many_to_many :groups, SunsetPickleball.Group, join_through: SunsetPickleball.PlayerGroup
    has_many :earned_points_ratios, SunsetPickleball.EarnedPointsRatio
    has_many :games, through: [:earned_points_ratios, :game]
  end

  @doc """
  Build a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:first_name, :last_name])
    |> validate_required([:first_name, :last_name])
  end

  @doc """
  Calculate the combined EPR for a player over all their games.
  """
  def total_epr(player) do
    Repo.preload(player, :earned_points_ratios).earned_points_ratios
    |> combine_eprs
  end

  @doc """
  Return every opponent a player has faced.
  """
  def opponents_played(player = %Player{}) do
    from(p in Player, where: p.id == ^player.id)
    |> opponents_played
    |> Repo.all
  end

  def opponents_played(query = %Ecto.Query{}) do
    from(
      p in query,
      join: s in assoc(p, :scores),
      join: os in Score, on: s.game_id == os.game_id and s.team_id != os.team_id,
      join: otr in Roster, on: os.team_id == otr.team_id,
      join: op in assoc(otr, :player),
      select: op,
      distinct: true
    )
  end

  @doc """
  Calculate the combined total of EPRs for a player's opponents
  """
  def opponents_epr(player) do
    opponents_played(player)
    |> Repo.preload(:earned_points_ratios)
    |> Stream.flat_map(&(&1.earned_points_ratios))
    |> combine_eprs
  end

  @doc """
  Calculate the total EPR for group games only
  """
  def group_epr(player, group) do
    Repo.preload(player, earned_points_ratios: :game).earned_points_ratios
    |> filter_by_group(group)
    |> combine_eprs
  end

  @doc """
  Return the number of games played in the given group
  """
  def group_games_played(player, group) do
    Repo.preload(player, :games).games
    |> Enum.filter(&(&1.group_id == group.id))
    |> length
  end

  defp filter_by_group(eprs, group) do
    eprs
    |> Stream.filter(fn epr ->
      epr.game.group_id == group.id
    end)
  end

  defp combine_eprs(eprs) do
    eprs
    |> Enum.reduce({0, 0}, fn (epr, {earned_points, total_points}) ->
      {earned_points + epr.earned_points, total_points + epr.total_points}
    end)
    |> case do
         {_, 0} -> nil
         {earned_points, total_points} -> earned_points / total_points
       end
  end
end
