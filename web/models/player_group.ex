defmodule SunsetPickleball.PlayerGroup do
  use SunsetPickleball.Web, :model

  schema "player_groups" do
    belongs_to :player, SunsetPickleball.Player
    belongs_to :group, SunsetPickleball.Group

    timestamps()
  end

  @required_fields [:player_id, :group_id]

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @required_fields)
    |> validate_required(@required_fields)
  end
end
