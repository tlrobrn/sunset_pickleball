defmodule SunsetPickleball.Game do
  alias SunsetPickleball.{Game, Group, Repo, Score}
  use SunsetPickleball.Web, :model

  schema "games" do
    belongs_to :group, SunsetPickleball.Group

    timestamps()

    has_many :scores, SunsetPickleball.Score
    has_many :earned_points_ratios, SunsetPickleball.EarnedPointsRatio
    many_to_many :teams, SunsetPickleball.Team, join_through: SunsetPickleball.Score
    many_to_many :players, SunsetPickleball.Player, join_through: SunsetPickleball.EarnedPointsRatio
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:group_id])
    |> validate_required([])
  end

  @doc """
  Returns the final score for each team in a game.
  """
  def final_scores(game) do
    Repo.preload(game, :scores).scores
    |> Enum.group_by(&(&1.team_id))
    |> Enum.map(fn {_team_id, scores} ->
      Enum.max_by(scores, &(&1.points))
    end)
  end

  def schedule(teams) do
    Game.changeset(%Game{}, %{})
    |> Repo.insert!
    |> create_scores(teams)
  end

  def schedule_for_group(group, teams) do
    Game.changeset(%Game{}, %{group_id: group.id})
    |> Repo.insert!
    |> create_scores(teams)
  end

  def upcoming_games(groups) do
    group_ids = groups |> Enum.map(&(&1.id))
    Repo.all(from g in incomplete_games, where: g.group_id in ^group_ids, order_by: g.id, preload: :group)
    |> Enum.group_by(&(&1.group_id))
    |> Enum.sort_by(fn {_group_id, games} -> length(games) end, &>=/2)
    |> Stream.map(fn {_group_id, [game | _]} -> game end)
    |> Enum.sort_by(&length(Group.completed_games(&1.group)))
  end

  defp incomplete_games do
    from g in Game,
      left_join: e in assoc(g, :earned_points_ratios),
      where: is_nil(e.id)
  end

  defp create_scores(game, [home_team, away_team]) do
    Repo.insert!(Score.changeset(%Score{}, %{game_id: game.id, team_id: home_team.id, points: 0, serve: "second"}))
    Repo.insert!(Score.changeset(%Score{}, %{game_id: game.id, team_id: away_team.id, points: 0, serve: "no"}))
    game
  end
end
