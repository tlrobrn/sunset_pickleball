defmodule SunsetPickleball.EarnedPointsRatio do
  alias SunsetPickleball.{EarnedPointsRatio, Game, Repo}
  use SunsetPickleball.Web, :model

  schema "earned_points_ratios" do
    field :earned_points, :integer
    field :total_points, :integer
    belongs_to :player, SunsetPickleball.Player
    belongs_to :game, SunsetPickleball.Game

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:earned_points, :total_points])
    |> validate_required([:earned_points, :total_points])
  end

  @doc """
  Calculates the earned point ratios for players given a game.
  """
  def calculate(game = %Game{}) do
    scores = Game.final_scores(game) |> Repo.preload(:players)
    total_points = Enum.reduce(scores, 0, &(&1.points + &2))

    scores
    |> Stream.flat_map(fn score ->
      Stream.map(score.players, fn player ->
        %EarnedPointsRatio{
          earned_points: score.points,
          total_points: total_points,
          player_id: player.id,
          game_id: game.id,
        }
      end)
    end)
    |> Enum.map(&Repo.insert!/1)
  end
end
