defmodule SunsetPickleball.Team do
  alias SunsetPickleball.{Player, Repo, Roster, Team}
  use SunsetPickleball.Web, :model

  schema "teams" do

    timestamps()

    has_many :scores, SunsetPickleball.Score
    many_to_many :players, SunsetPickleball.Player, join_through: SunsetPickleball.Roster
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [])
    |> validate_required([])
  end

  @doc """
  Returns a team, if one exists, for the given players.
  """
  def for_players(query, []), do: query
  def for_players(query, [player | players]) do
    for_players(for_player(query, player), players)
  end

  defp for_player(query, player) do
    from t in query,
      join: r in Roster, on: t.id == r.team_id,
      join: p in Player, on: r.player_id == p.id,
      where: p.id == ^player.id
  end

  @doc """
  Returns a team for the given players.

  If none exists, then a new one will be created.
  """
  def fetch_or_create(players) do
    case Repo.one(for_players(Team, players)) do
      nil -> create_for(players)
      team -> team
    end
  end

  defp create_for(players) do
    team = Team.changeset(%Team{}, %{})
    |> Repo.insert!

    players
    |> Stream.map(&Roster.changeset(%Roster{}, %{team_id: team.id, player_id: &1.id}))
    |> Enum.map(&Repo.insert!/1)

    team
  end
end
