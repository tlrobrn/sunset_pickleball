defmodule SunsetPickleball.Group do
  alias SunsetPickleball.{Game, Group, PlayerGroup, Repo, Team}
  use SunsetPickleball.Web, :model

  schema "groups" do
    field :name, :string

    timestamps()

    has_many :player_groups, SunsetPickleball.PlayerGroup
    has_many :games, SunsetPickleball.Game
    has_many :players, through: [:player_groups, :player]
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name])
    |> validate_required([])
  end

  @doc """
  Generates games allowing for balanced matches in a group.
  With 4 players there are 3 games
  With 5 players there are 5 games
  """
  def generate_games(struct) do
    group = Repo.preload(struct, :players)
    fetch_or_create_teams(group.players)
    |> Stream.chunk(2)
    |> Enum.map(&Game.schedule_for_group(group, &1))
  end

  @doc """
  Generates groups of 4 or 5
  """
  def generate_groups(players) do
    group_players = players
    |> Enum.shuffle
    |> chunk_players

    Stream.iterate(1, &(&1 + 1))
    |> Stream.map(&Integer.to_string/1)
    |> Stream.take(length(group_players))
    |> Stream.map(&Group.changeset(%Group{}, %{name: &1}))
    |> Stream.map(&Repo.insert!/1)
    |> Stream.zip(group_players)
    |> Enum.map(&associate_players/1)
  end

  def associate_players(group, player_ids) do
    player_ids
    |> Enum.each(fn player_id ->
      PlayerGroup.changeset(%PlayerGroup{}, %{player_id: player_id, group_id: group.id})
      |> Repo.insert!
    end)

    group
  end

  defp associate_players({group, players}) do
    player_ids = players |> Stream.map(&(&1.id))
    associate_players(group, player_ids)
  end

  defp fetch_or_create_teams([a, b, c, d]) do
    [
      Team.fetch_or_create([a, b]),
      Team.fetch_or_create([c, d]),

      Team.fetch_or_create([b, c]),
      Team.fetch_or_create([a, d]),

      Team.fetch_or_create([a, c]),
      Team.fetch_or_create([b, d]),
    ]
  end

  defp fetch_or_create_teams([a, b, c, d, e]) do
    [
      Team.fetch_or_create([a, b]),
      Team.fetch_or_create([c, e]),

      Team.fetch_or_create([b, c]),
      Team.fetch_or_create([a, d]),

      Team.fetch_or_create([c, d]),
      Team.fetch_or_create([b, e]),

      Team.fetch_or_create([d, e]),
      Team.fetch_or_create([a, c]),

      Team.fetch_or_create([e, a]),
      Team.fetch_or_create([b, d]),
    ]
  end

  defp chunk_players(players) when rem(length(players), 4) == 0 do
    players |> Enum.chunk(4)
  end

  defp chunk_players(players) when rem(length(players), 5) == 0 do
    players |> Enum.chunk(5)
  end

  defp chunk_players(players) when rem(length(players), 4) == 1 do
    {first_group, remaining_players} = players |> Enum.split(5)
    [first_group | chunk_players(remaining_players)]
  end

  defp chunk_players(players) when rem(length(players), 4) == 2 do
    {first_group, remaining_players} = players |> Enum.split(10)
    chunk_players(first_group) ++ chunk_players(remaining_players)
  end

  defp chunk_players(players) when rem(length(players), 4) == 3 do
    {first_group, remaining_players} = players |> Enum.split(15)
    chunk_players(first_group) ++ chunk_players(remaining_players)
  end

  def completed_games(group) do
    Repo.preload(group, games: :earned_points_ratios).games
    |> Enum.filter(&has_earned_points_ratios/1)
  end

  defp has_earned_points_ratios(game), do: check_eprs(game.earned_points_ratios)

  defp check_eprs([]), do: false
  defp check_eprs(_), do: true
end
