defmodule SunsetPickleball.Roster do
  use SunsetPickleball.Web, :model

  schema "rosters" do
    belongs_to :team, SunsetPickleball.Team
    belongs_to :player, SunsetPickleball.Player

    timestamps()
  end

  @required_fields [:team_id, :player_id]

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @required_fields)
    |> validate_required(@required_fields)
  end
end
