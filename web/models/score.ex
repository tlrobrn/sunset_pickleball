defmodule SunsetPickleball.Score do
  alias SunsetPickleball.Score
  use SunsetPickleball.Web, :model

  schema "scores" do
    field :points, :integer
    field :serve, :string
    belongs_to :game, SunsetPickleball.Game
    belongs_to :team, SunsetPickleball.Team

    timestamps()

    has_many :players, through: [:team, :players]
  end

  @required_fields [:points, :serve, :game_id, :team_id]
  @serve_enum ["no", "first", "second"]

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @required_fields)
    |> validate_required(@required_fields)
    |> validate_inclusion(:serve, @serve_enum)
  end

  @doc """
  Builds a query for scores with the given ids.
  """
  def all_for_ids(ids) do
    from(s in Score, where: s.id in ^ids)
  end
end
