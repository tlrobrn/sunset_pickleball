defmodule SunsetPickleball.Router do
  use SunsetPickleball.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", SunsetPickleball do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    resources "/players", PlayerController
    get "/groups/generate", GroupController, :generate
    post "/groups/generate", GroupController, :generate_groups
    resources "/groups", GroupController
    resources "/games", GameController
    resources "/scores", ScoreController
    resources "/earned_points_ratios", EarnedPointsRatioController
  end
end
