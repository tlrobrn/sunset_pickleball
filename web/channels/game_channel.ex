defmodule SunsetPickleball.GameChannel do
  alias SunsetPickleball.{EarnedPointsRatio, Game, Score}
  use SunsetPickleball.Web, :channel

  def join("game:" <> game_id, payload, socket) do
    if authorized?(payload) do
      {game_id, ""} = Integer.parse(game_id)
      send(self, {:after_join, game_id})
      {:ok, assign(socket, :game_id, game_id)}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  def handle_info({:after_join, game_id}, socket) do
    game = Game
    |> Repo.get(game_id)
    |> Repo.preload([:earned_points_ratios, :scores, teams: :players])

    players = game.teams
    |> Stream.flat_map(fn team ->
      team.players
      |> Enum.map(fn player ->
        %{
          "team_id" => team.id,
          "first_name" => player.first_name,
          "last_name" => player.last_name,
        }
      end)
    end)
    |> Enum.uniq

    scores = game.scores
    |> Enum.sort_by(&(&1.id))
    |> Enum.map(&score_payload/1)

    push(socket, "players", %{"players" => players})
    push(socket, "scores", %{"scores" => scores})
    if length(game.earned_points_ratios) > 0, do: push(socket, "game_over", %{})

    {:noreply, socket}
  end

  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  def handle_in("scores", %{"scores" => scores}, socket) do
    scores
    |> Stream.map(&get_changeset/1)
    |> Stream.map(&Repo.insert/1)
    |> collapse_inserts(socket)
  end

  def handle_in("undo", %{"scores" => scores} = payload, socket) do
    scores
    |> Enum.map(&(&1["id"]))
    |> Score.all_for_ids
    |> Repo.delete_all

    broadcast!(socket, "undo", payload)
    {:reply, :ok, socket}
  end

  def handle_in("game_over", _payload, socket) do
    Game
    |> Repo.get(socket.assigns[:game_id])
    |> EarnedPointsRatio.calculate

    broadcast!(socket, "game_over", %{})
    {:reply, :ok, socket}
  end

  defp get_changeset(params) do
    params
    |> (fn p -> Score.changeset(%Score{}, p) end).()
  end

  defp collapse_inserts(stream, socket) do
    scores = stream
    |> Enum.reduce([], fn ({:ok, score}, scores) -> [score | scores] end)
    |> Enum.map(&score_payload/1)

    broadcast!(socket, "scores", %{"scores" => scores})
    {:reply, :ok, socket}
  end

  defp score_payload(%Score{} = score) do
    %{
      "id" => score.id,
      "points" => score.points,
      "serve" => score.serve,
      "game_id" => score.game_id,
      "team_id" => score.team_id,
    }
  end

  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end
end
