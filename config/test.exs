use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :sunset_pickleball, SunsetPickleball.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :sunset_pickleball, SunsetPickleball.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "sunset_pickleball_test",
  hostname: System.get_env("SUNSET_PICKLEBALL_DB_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
