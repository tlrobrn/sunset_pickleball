# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :sunset_pickleball,
  ecto_repos: [SunsetPickleball.Repo]

# Configures the endpoint
config :sunset_pickleball, SunsetPickleball.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "XmmKYNhm5fSvH5wdnd73kGk+qX4EKEmxr1I4o7ZXEq7D2p7ZFnJa5uQopwQgKokC",
  render_errors: [view: SunsetPickleball.ErrorView, accepts: ~w(html json)],
  pubsub: [name: SunsetPickleball.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
