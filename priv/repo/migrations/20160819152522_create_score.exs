defmodule SunsetPickleball.Repo.Migrations.CreateScore do
  use Ecto.Migration

  def change do
    execute("CREATE TYPE serve AS ENUM ('no', 'first', 'second')")
    create table(:scores) do
      add :points, :integer
      add :serve, :serve, null: false
      add :game_id, references(:games, on_delete: :nothing)
      add :team_id, references(:teams, on_delete: :nothing)

      timestamps()
    end
    create index(:scores, [:game_id])
    create index(:scores, [:team_id])

  end
end
