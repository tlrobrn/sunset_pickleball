defmodule SunsetPickleball.Repo.Migrations.CreateEarnedPointsRatio do
  use Ecto.Migration

  def change do
    create table(:earned_points_ratios) do
      add :earned_points, :integer
      add :total_points, :integer
      add :player_id, references(:players, on_delete: :nothing)
      add :game_id, references(:games, on_delete: :nothing)

      timestamps()
    end
    create index(:earned_points_ratios, [:player_id])
    create index(:earned_points_ratios, [:game_id])

  end
end
