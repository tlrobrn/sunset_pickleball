defmodule SunsetPickleball.Repo.Migrations.CreateGame do
  use Ecto.Migration

  def change do
    create table(:games) do
      add :group_id, references(:groups, on_delete: :nothing)

      timestamps()
    end
    create index(:games, [:group_id])

  end
end
